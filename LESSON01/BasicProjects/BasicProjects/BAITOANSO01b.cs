﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicProjects
{
    class BAITOANSO01b
    {
        //Đây là 1 hàm có tên là Demo
        public static void Demo()
        {
            //Ví dụ về Nested If..Else Statement
            //loat dtb = 6.5f;
            float dtb = (float)6.5;

            if (dtb < 5.0)
                Console.WriteLine("Học lực Yếu");
            else
            {
                if (dtb >= 5.0 && dtb < 6.5)
                    Console.WriteLine("Học lực Trung Bình");
                else
                {
                    if (dtb >= 6.5 && dtb < 7.5)
                        Console.WriteLine("Học lực TB Khá");
                    else
                        Console.WriteLine("...");
                }
            }
        }
    }
}
