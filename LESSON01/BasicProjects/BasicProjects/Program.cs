﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicProjects
{
    class Program
    {
        static void Main(string[] args)
        {
            //Thiết lập hiện thị Unicode tại cửa sổ kết quả
            Console.OutputEncoding = Encoding.UTF8;

            //Sử dụng để gọi tới các bài tập đã thực hiện
            //Example01.Demo();
            Example03.Demo();

            //Giữ lại màn hình kết quả để theo dõi
            Console.ReadLine();
        }
    }
}
