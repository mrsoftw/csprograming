﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicProjects
{
    class Example03
    {
        //Đây là 1 hàm có tên là Demo
        public static void Demo()
        {
            //Ví dụ về If Single Statement
            byte a = 0;
            //a = 10;
            if(a > 0)           
                Console.WriteLine("Giá trị của a > 0");            
            else
                Console.WriteLine("Giá trị của a <= 0");
        }
    }
}
